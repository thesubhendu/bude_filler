
window.baseApiUrl = `https://v2.stabilityhealthcare.com/api`;
// window.baseApiUrl = `http://hbude.test/api`;


document.addEventListener("DOMContentLoaded", function () {
    //smenh80@gmail.com

    chrome.storage.sync.get(['token'], function (result) {
        let token = result.token;

        if (token) {
            showNurseEmailForm();
            getAllSubmissions();
        }

    });


});



function showNurseEmailForm() {
    document.querySelector('#loginform').style.display = 'none';
    document.querySelector('#mainPopup').style.display = 'block';
}


function showLoginForm() {
    document.querySelector('#loginform').style.display = 'block';
    document.querySelector('#mainPopup').style.display = 'none';
}

function showErrorMessage(message) {
    let errorBox = document.querySelector('#errorsContainer');

    errorBox.textContent = message;
    errorBox.style.display = 'block';



    setTimeout(function () {
        errorBox.style.display = 'none';
    }, 3000)

}

function showSuccessMessage(message) {
    let successBox = document.querySelector('#successContainer');

    successBox.textContent = message;
    successBox.style.display = 'block';

    let infoBox = document.querySelector('#infoContainer');

    if (infoBox) {
        infoBox.style.display = 'none';
    }


}

function showInfoMessage(message) {
    let infoBox = document.querySelector('#infoContainer');

    infoBox.textContent = message;
    infoBox.style.display = 'block';

    setTimeout(function () {
        infoBox.style.display = 'none';
    }, 3000)

}


function debounce(func, wait, immediate) {
    var timeout;
    return function () {
        var context = this, args = arguments;
        var later = function () {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
};


document.querySelector('#login').addEventListener('click', function () {


    let email = password = '';

    email = document.querySelector('#your_email').value;
    password = document.querySelector('#password').value;


    let budeLoginApi = `${baseApiUrl}/login`;
    showInfoMessage('Logging In Please Wait');

    fetch(budeLoginApi, {
        method: 'POST',
        mode: 'cors',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({ email: email, password: password })
    }).then(response => {
        return response.json();
    }).then(data => {

        if (data.error) {
            showErrorMessage("Wrong Credentials");
            return false;
        }


        let token = data.access_token;

        chrome.storage.sync.set({ token: token }, function () {

            //login success
            showNurseEmailForm();
            showSuccessMessage('Log in Success!');
            getAllSubmissions();

        });

    });

});

document.querySelector('#fillData').addEventListener('click', function () {

    let submission = document.querySelector('#submissions_list');

    getCustomerData(submission.value);

});

function sendCustomerDataToPopulate(customerData) {

    chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
        let currentTab = tabs[0];
        let tabUrl = currentTab.url;
        let siteMainUrl = tabUrl.split('/')[2];
        let formToFill = 'aya'; //default aya

        if (siteMainUrl == 'vms.medefis5.com') {
            formToFill = 'medifis';
        }

        chrome.tabs.sendMessage(currentTab.id, { customer: customerData, formToFill: formToFill });
        showSuccessMessage(formToFill.toUpperCase() + ' Data populated!');

    });
}


function getAllSubmissions() {
    let emailListWrap = document.querySelector('#submissions_list');

    emailListWrap.innerHTML = '';


    chrome.storage.sync.get(['token'], function (result) {
        let token = result.token;

        if (token.length == 0) {
            showErrorMessage("Please Login!")
            showLoginForm();

            return false;
        }



        let suggestionApi = `${baseApiUrl}/nurses?token=${token}`;


        fetch(suggestionApi,{
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }).then(res => {

            if (res.status == 401) {
                revokeToken();
                throw new Error("Unauthenticated");
            }

            return res.json();

        }).then(response => {

            let data = response.data;

            emailListWrap.innerHTML = "";

            data.forEach(submission => {
                emailListWrap.innerHTML += `<option value="${submission.submission_id}"> ${submission.customer.first_name} (${submission.job.account.account_name})</option>`;
            })


        }).catch(error => {

            showErrorMessage('Whoops something went wrong!')

        });


    });


}

function revokeToken() {
    chrome.storage.sync.remove(['token'], function (result) {
        showLoginForm();
    });
}

function getCustomerData(submissionId) {


    chrome.storage.sync.get(['token'], function (result) {
        let token = result.token;

        if (token.length == 0) {
            showErrorMessage("Please Login!")
            showLoginForm();

            return false;
        }

        let suggestionApi = `${baseApiUrl}/nurses?token=${token}&submission_id=${submissionId}`;


        fetch(suggestionApi,{
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }).then(res => {

            if (res.status == 401) {
                revokeToken();
                throw new Error("Unauthenticated");
            }

            return res.json();

        }).then(response => {
            let data = response;

            sendCustomerDataToPopulate(data);

        }).catch(error => {

            showErrorMessage('Whoops something went wrong!')
        });
    });

}


document.querySelector('#refresh').addEventListener('click', function () {

    getAllSubmissions();
});