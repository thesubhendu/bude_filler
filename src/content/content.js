
chrome.runtime.onMessage.addListener(
	function (message, sender, sendResponse) {
		
		let data = message.customer.data;

		let formToFill = message.formToFill;

		let nonFillableData = data.non_fillable[formToFill];

		injectJson(nonFillableData);

		if(formToFill == 'medifis') {
			fillMedifisForm(data);
		}else {
			fillAyaForm(data);
		}


	});


function injectJson(data){

	let body = document.querySelector('body');
	let jsonViewerDiv = document.createElement('div');
	jsonViewerDiv.setAttribute('id', 'json-renderer');
	body.prepend(jsonViewerDiv);

	$('#json-renderer').jsonViewer(data,{collapsed: false, withQuotes: true, withLinks: false});
}

function fillAyaForm(data) {

	let customer = data.submission.customer;

	let asd = data.available_start_date;


	$('#FirstName').val(customer.first_name);
	$('#LastName').val(customer.last_name);
	$('#Email').val(customer.email);


	$('#Address1').val(customer.detail.address_1);
	$('#Address2').val(customer.detail.address_2);
	$('#City').val(customer.detail.city);
	$('#Zip').val(customer.detail.zip);
	$('#SellingPoints').val(customer.candidate_summary);

	document.querySelector('#CanTravel').checked =true;

	$('[name=contact]').value = customer.phone;


	document.querySelector('label[for=AvailableStartDate]').closest('div').querySelector('input').value= asd;
	document.querySelector('label[for=DOB]').closest('div').querySelector('input').value= customer.detail.dob;
	document.querySelector('label[for=SSN]').closest('div').querySelector('input').value= customer.detail.ssn;


	//getStateValue() comes from /js/utils/aya_state_map.js included in manifest.json

	document.querySelector('label[for=State]').closest('div').querySelector('select').value= getStateVal(customer.detail.state);

	// let jobShift = data.submission.job.shift_category;
	// document.querySelector('label[for=PreferredShift]').closest('div').querySelector('select').value= getAyaShiftVal(jobShift);

}

// function getAyaShiftVal(shift){

// 	const lookup = {
// 		"days": "days",
// 		"nights": "night",
// 	}

// 	return lookup[shift];
// }

function fillMedifisForm(data) { 


	let customer = data.submission.customer;
	

	let mapping = {
		firstName : 'first_name',
		lastName : 'last_name',
		email : 'email',
		city : 'city',
		state : 'state',
		ssn : 'ssn',
		zipCode : 'zip',
		dateOfBirth : 'dob',
		cellPhone : 'phone',
		address1 : 'address_1',
		address2 : 'address_2',
		yearsOfExperience : 'travel_experience',
		comments : 'candidate_summary',
	};

	
	Object.keys(mapping).forEach(key => {

		let budeCustomerTableColumn = mapping[key];
		let formInputFieldName = key;

		let dbValue = customer[budeCustomerTableColumn];

		if(!dbValue) {
			dbValue = customer.detail[budeCustomerTableColumn];
		}


		document.querySelector(`[name='${formInputFieldName}']`).value = dbValue;

	});

	document.querySelector('[name=isWillingToRelocate]').checked = true;
	document.querySelector('[name=timeZoneId]').value = 7; //pacific standard time


	//fill work histories
	let pastHospitals = customer.work_histories.map(val => val.hospital);

	
	let hospitalField = document.querySelector('[name=newFacility]');

	pastHospitals.forEach((hospitalName,index)=>{

		setTimeout(function(){
			hospitalField.value = hospitalName; 
			hospitalField.closest('div').querySelector('.btn').disabled = false; //click add facility button
			setTimeout(function(){
				hospitalField.closest('div').querySelector('.btn').click(); //click add facility button
			}, 1000*index);

		}, 1000*index);

	})


	//fill licenses

	customer.license.forEach((license,index) => {

		setTimeout(function(){
			document.querySelector('[name=licenseStateCode]').value = license.state ; 
			document.querySelector('[name=licenseStatusId]').value = 281 ; //active , 282 inactive 
			document.querySelector('[name=licenseNumber]').value = license.license_no ; 
	
			document.querySelector('[name=licenseNumber]').closest('div').querySelector('.btn').disabled=false; //click add license button
			
			
			setTimeout(function(){
				document.querySelector('[name=licenseNumber]').closest('div').querySelector('.btn').click(); //click add license button
			}, 1000*index);


		}, 1000*index);
	})

	

}

